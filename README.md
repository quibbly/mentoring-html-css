# Mentoring-HTML-CSS
## Kristina Davletshina
## _Achievements_:
   * Led a team of 4 QA engineers
   * Boosted team’s productivity by 20%
   * Increased customer satisfaction and product loyalty by 15%
### _Other_:
   * Quick learner
   * Effective leader
   * Strong communication and interpersonal skills
